const start = document.querySelector<HTMLButtonElement>("#start")
const debut = document.querySelector<HTMLElement>("#debut")
const quest = document.querySelector<HTMLElement>("h3")
const span = document.querySelectorAll<HTMLElement>("span")
const faute = document.querySelector<HTMLElement>("#wrong")
const fin = document.querySelector<HTMLStyleElement>("#fin")
const quizz = document.querySelector<HTMLElement>("#quizz")
const next = document.querySelector<HTMLButtonElement>("#next")
const choix = document.querySelectorAll<HTMLElement>(".choix")
const info = document.querySelector<HTMLElement>("#infoPlus")

let score: number = 0;
let wrong: number = 0;
let numQuestion: number = 0;

next.style.display = "none";
info.style.display = "none";
quizz.style.display = "none";
fin.style.display = "none";

let question: string[] = ["Selon la mythologie grecque, lorsqu'un individu décède et atteint le royaume des morts, il se trouve face à un fleuve. Quel est le nom de ce fleuve ?", "Quelle fonction Cerbère occupait-il dans la mythologie grecque ?", "Qui est le Roi des dieux dans la mythologie grecque ?", "De quoi Poséidon est t’il le dieu dans la mythologie grecque?", "Par qui Méduse a-t-elle été maudite dans la mythologie grecque ?", "Que donne Ariane à Thésée pour pouvoir s'échapper du labyrinthe du Minotaure?", "Quel nom donne-t-on aux trois déesses du destin dans la mythologie grecque ?"]

let option = [["Le Styx", "Le Strymon", "Le Nestos", "Le Amnisos"],
["Gardien du monde des vivant", "Chef des chien gardien de troupeau", "Gardien de l’entrer du monde des dieux", "Gardien de l’entrer des enfers"],
["Chonos", "Zeus", "Ouranos", "Hades"],
["Dieu des fleuves et des rivières", "Dieu de la mer et des tempêtes", "Dieu de la pluis et du vent", "Dieu du ciel et des tempêtes"],
["Héras", "Hestia", "Athéna", "Perséphone"], 
["une épée", "une carte", "Une bobine de fil", "une torche"],
["Les prêtresses du destin", "Les Moirae", "Les moines", "Les soeurs de la vie"]]

let reponce: string[] = ["Le Styx", "Gardien de l’entrer des enfers", "Zeus", "Dieu de la mer et des tempêtes", "Athéna", "Une bobine de fil", "Les Moirae"]

let infos: string[] = ["C’est le Styx. Afin de payer Charon, le passeur, les morts étaient inhumés avec une pièce dans la bouche pour pouvoir traverser les eaux sombres et atteindre le royaume des Morts. Dans le cas contraire, les âmes restent coincées sur ces rives.", "Cerbère le chien à trois têtes garde l'entrée des Enfers son rôle principal était d'empêcher les âmes des défunts de quitter le royaume des morts.", "Le roi des dieux dans la mythologie grecque est Zeus dieu du ciel, de la foudre.", "Poséidon est le dieu de la mer, des tremblements de terre et des tempêtes dans la mythologie grecque.", "Méduse était autrefois une belle prêtresse du temple d'Athéna, après avoir été séduite par Poséidon dans le temple, Athéna, transforma Méduse en une créature hideuse aux cheveux de serpents et quiconque croisait son regard était instantanément transformé en pierre.", "C'est une bobine de fil qu'Arine donne à Thésée, en lui disant d'attaché les bouts du fil à l'entrée pour ensuite le suivre en sens inverse au moment de sortir. Cette bobine est plus connue sous le nom de fil d'Ariane.", "On les appelle les Trois Moirae ! Clotho, la plus jeune, tisse le fil de la vie. Elle en est l'origine même, la création de la vie elle-même. Lachesis, la deuxième soeur, est celui qui décide du sort des gens au cours de leur vie. La dernière soeur et Atropos coupe le fil de la vie et avec ses ciseaux, elle détermine comment quelqu'un va mourir."]

start.addEventListener("click", () => {
  debut.style.display = "none";
  quizz.style.display = "block";
  update()
})

for (const [i, choixRep] of choix.entries()) {
  choixRep.addEventListener("click", () => {
    if (option[numQuestion][i] == reponce[numQuestion]) {
      choixRep.classList.add("true")
      checkAnswer(true)

    };
    if (option[numQuestion][i] != reponce[numQuestion]) {
      choixRep.classList.add("false")
      checkAnswer(false)
    };
  });
}

next.addEventListener("click", () => {
  numQuestion++
  update()
})

function checkAnswer(correct:boolean) {
  if (correct == true){
    score++;
  } else {
    wrong++;
  }
  next.style.display = "block";
  info.style.display = "block";
  quest.style.display = "none";
}

function update() {
  next.style.display = "none";
  info.style.display = "none";
  quest.style.display = "block";


  if (info) {
    info.innerText = infos[numQuestion]
  }

  if (question.length > numQuestion) {
    quizz.style.display = "block";
    fin.style.display = "none";
  } else {
    quizz.style.display = "none";
    fin.style.display = "block";
  }

  if (span) {
    for (const item of span) {
      item.textContent = score + "";
    }
  }
  faute.textContent = wrong + "";

  if (quest) {
    quest.innerText = question[numQuestion]
  }
  for (const [i, choixRep] of choix.entries()) {
    choixRep.innerText = option[numQuestion][i]
    choixRep.classList.remove("true")
    choixRep.classList.remove("false")
  }
}