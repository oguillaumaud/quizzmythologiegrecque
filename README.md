Projet Quiz Mythologie Grecque

J'ai choisi comme thème pour ce quiz la mythologie grecque.
Le projet utilise Typescript et l'outil de build Vite. 

Pour le réaliser j'ai choisi de faire plusieurs tableaux, un pour les questions, une pour les choix possibles un pour les reponce juste et pour finir un pour des informations supplémentaires liées à la question posée.
Pour l'organisation j'ai choisi de faire 3 parties différentes qui s'alterne pendant que l'une est visible les autres sont cachés. La première au début avant de lancer les quiz, la suivante contient le quiz et la dernière nous montre notre résultat.

J'ai créé 2 fonctions, une pour vérifier si les reponce aux questions sont correctes ou non cela applique un style différents en fonction de la reponce et ajoute une point à nôtre score de bonne réponce ou un point au score de mauvaise reponce.
La deuxième fonction sert à passer d'une question à l'autre et à metre à jour le score respectif de bonne ou mauvaise reponce. Elle a aussi pour fonction de vérifier si les quiz se terminent ou non et ainsi passé à las page de fin quand cela arrive, a l'aide d'une comparaison avec le numéro de la question et la taille du tableau de question.

Deux eventsListener permet de commencer le quiz ou de passé a la question suivante lorsque l'on click sur un bouton.￼
Le même procéder est utiliser pour le choix des question et déclancher la vérification de la réponce.

Pour actualiser les reponce que l'on peut choisir un ForOF et utiliser, cela permet de sélectionner plusieurs éléments identique mais qui doive contenir des contenus différents.
Le même procéder est utilisé pour le choix des questions et déclencher la vérification de la reponce.

Maquette Deckstop:

![Maquette du départ du quiz](src/img/maquetteStart.png)
![Maquette du départ du quiz](src/img/maquetteQuiz.png)
![Maquette du départ du quiz](src/img/maquetteEnd.png)

Maquette mobile:

![Maquette du départ du quiz](src/img/maquetteMobileStart.png)
![Maquette du départ du quiz](src/img/maquetteMobileQuiz.png)
![Maquette du départ du quiz](src/img/maquetteMobileEnd.png)